#ifndef _APM_GRAPH_PROPERTIES_H_
#define _APM_GRAPH_PROPERTIES_H_
/**
 * \file apm_graph_properties.h
 * \brief
 *  	This file contains APM container commands and events structures definition
 *
 * Copyright (c) 2019-2020 Qualcomm Innovation Center, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted (subject to the limitations in the
 * disclaimer below) provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     * Neither the name of Qualcomm Innovation Center nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 * GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 * HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// clang-format off
/*
$Header: //components/rel/avs.fwk/1.0/api/apm/apm_graph_properties.h#11 $
*/
// clang-format on

#include "ar_defs.h"

#ifdef __cplusplus
extern "C"
{
#endif /*__cplusplus*/

/** Legacy Container Capability IDs. To be removed. */
/**< Enumeration for container capability ID Pre/Post
  Processing (PP) */
#define APM_CONTAINER_CAP_ID_PP                   0x1

/**< Enumeration for container capability ID
  Compression/Decompression (CD) */
#define APM_CONTAINER_CAP_ID_CD                   0x2

/**< Enumeration for container capability ID End Point(EP) */
#define APM_CONTAINER_CAP_ID_EP                   0x3

/**< Enumeration for container capability ID Offload (OLC) */
#define APM_CONTAINER_CAP_ID_OLC                  0x4


/**< Enumeration for container type for Specialized Container */
#define APM_CONTAINER_TYPE_ID_SC                   0x0B001000

/**< Enumeration for container type for Generic Container */
#define APM_CONTAINER_TYPE_ID_GC                   0x0B001001

/**< Enumeration for container type OffLoad Container (OLC) */
#define APM_CONTAINER_TYPE_ID_OLC                  0x0B001002


/////////////////DOMAIN IDs//////////////////////////

/**  THESE HAVE TO BE SAME AS GPR IDs
  refer gpr_ids_domains.h */

/**< Enumeration for container processor domain for
  Invalid Domain */
#define APM_PROC_DOMAIN_ID_INVALID                0x0

/**< Enumeration for container procesor domain
  Modem DSP */
#define APM_PROC_DOMAIN_ID_MDSP                   0x1

/**< Enumeration for container processor domain for
  Audio DSP */
#define APM_PROC_DOMAIN_ID_ADSP                   0x2

/**< Enumeration for container procesor doman
  Sensors DSP */
#define APM_PROC_DOMAIN_ID_SDSP                   0x4

/**< Enumeration for container procesor doman
  Compute DSP */
#define APM_PROC_DOMAIN_ID_CDSP                   0x5

////////////////////////////////////////////////////////
/**< Enumeration for invalid PROP ID */
#define APM_PROP_ID_INVALID                       0x0

/**< Enumeration for don't care ID */
#define APM_PROP_ID_DONT_CARE AR_NON_GUID(0xFFFFFFFF)

#ifdef __H2XML__
// Enums required for module H2XML generation.
enum processors {
   PROC_DOMAIN_ADSP = APM_PROC_DOMAIN_ID_ADSP,  /**< @h2xmle_name {ADSP} */
   PROC_DOMAIN_MDSP = APM_PROC_DOMAIN_ID_MDSP,  /**< @h2xmle_name {MDSP} */
   PROC_DOMAIN_SDSP = APM_PROC_DOMAIN_ID_SDSP,  /**< @h2xmle_name {SDSP} */
   PROC_DOMAIN_CDSP = APM_PROC_DOMAIN_ID_CDSP   /**< @h2xmle_name {CDSP} */
};

enum containerCapOld {
   APM_CONTAINER_CAP_INVALID_OLD = APM_PROP_ID_INVALID,  /**< @h2xmle_name {INVALID} */
   APM_CONTAINER_CAP_PP = APM_CONTAINER_CAP_ID_PP, 		/**< @h2xmle_name {PP} */
   APM_CONTAINER_CAP_CD = APM_CONTAINER_CAP_ID_CD, 		/**< @h2xmle_name {CD} */
   APM_CONTAINER_CAP_EP = APM_CONTAINER_CAP_ID_EP, 		/**< @h2xmle_name {EP} */
   APM_CONTAINER_CAP_OLC = APM_CONTAINER_CAP_ID_OLC,   	/**< @h2xmle_name {OLC} */
};

enum containerCap {
   APM_CONTAINER_TYPE_INVALID = APM_PROP_ID_INVALID,    /**< @h2xmle_name {Invalid} */
   APM_CONTAINER_TYPE_SC  = APM_CONTAINER_TYPE_ID_SC,   /**< @h2xmle_name {Specialized} @h2xmle_replace{APM_CONTAINER_CAP_ID_PP} */
   APM_CONTAINER_TYPE_GC  = APM_CONTAINER_TYPE_ID_GC,   /**< @h2xmle_name {Generic}     @h2xmle_replace{APM_CONTAINER_CAP_ID_CD, APM_CONTAINER_CAP_ID_EP}  */
   APM_CONTAINER_TYPE_OLC = APM_CONTAINER_TYPE_ID_OLC,  /**< @h2xmle_name {Offload}     @h2xmle_replace{APM_CONTAINER_CAP_ID_OLC}*/
};
#endif

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif /* _APM_GRAPH_PROPERTIES_H_ */
